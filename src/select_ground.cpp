// val = new double *[lin] {};
// 			for(int i = 0; i < lin; i++)
// 				val[i] = new double[col] {};
// 		}
		
		
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <iostream>
#include <pcl/io/pcd_io.h>

#include <pcl/PCLPointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

#include <vector>
#include <cmath>

typedef struct {
	int n_points = 0;
	std::vector<pcl::PointXYZI> points;
	float mean_height = 0;
	float mean_dev = 0;
	bool ground_flag = false;
} cellPoint;

void cellPointMean(cellPoint *cp) {
	if(cp->n_points != 0) {
		for(int i = 0; i < cp->n_points; i++) {
			cp->mean_height += cp->points[i].z;
		}
		cp->mean_height /= cp->n_points;
	}
}

void cellPointMeanDev(cellPoint *cp) {
	if(cp->n_points != 0) {
		for(int i = 0; i < cp->n_points; i++) {
			cp->mean_dev += std::abs(cp->points[i].z - cp->mean_height);
		}
		cp->mean_dev /= cp->n_points;
	}
}

ros::Publisher pub;
float cell_size_, obstacle_dev_,
	  ground_dev_, ground_height_, ground_tol_;
int grid_size_;

void callback(const sensor_msgs::PointCloud2ConstPtr &input) {
	pcl::PointCloud<pcl::PointXYZI> cloud;
	pcl::fromROSMsg(*input, cloud);
	
	int i, j, k;
	
	//Define a 2D grid
	cellPoint **grid = new cellPoint *[grid_size_] {};
	for(i = 0; i < grid_size_; i++) {
		grid[i] = new cellPoint [grid_size_] {};
	}
	
	//Build the grid from the PointCloud
	for(i = 0; i < cloud.width; i++) {
		int x = int(grid_size_ * cell_size_ / 2 - cloud.points[i].x),
			y = int(grid_size_ * cell_size_ / 2 - cloud.points[i].y);
		if(x >= 0 && x < grid_size_ * cell_size_ && y >= 0 && y < grid_size_ * cell_size_) {
			cloud.points[i].intensity = 0;
			grid[x][y].points.push_back(cloud.points[i]);
			grid[x][y].n_points++;
		}
	}
	
	//Compute mean and mean deviation
	for(i = 0; i < grid_size_; i++) {
		for(j = 0; j < grid_size_; j++) {
			cellPointMean(&(grid[i][j]));
			cellPointMeanDev(&(grid[i][j]));
			//Flag ground
			if(grid[i][j].mean_dev <= ground_dev_) {
				if(std::abs(grid[i][j].mean_height - ground_height_) < ground_tol_) {
					grid[i][j].ground_flag = true;
					for(k = 0; k < grid[i][j].n_points; k++) {
						grid[i][j].points[k].intensity = 255;
					}
				}
			}
			//Remove obstacles
			else if(grid[i][j].mean_dev > obstacle_dev_) {
				grid[i][j].n_points = 0;
			}
		}
	}
	
	//Rebuild the PointCloud
	pcl::PointCloud<pcl::PointXYZI> temp_cloud = cloud;
	temp_cloud.width = 0;
	temp_cloud.points.clear();

	for(i = 0; i < grid_size_; i++) {
		for(j = 0; j < grid_size_; j++) {
			for(k = 0; k < grid[i][j].n_points; k++) {
				temp_cloud.points.push_back(grid[i][j].points[k]);
				temp_cloud.width++;
			}
		}
	}
	
	//Deallocate the grid
	for(i = 0; i < grid_size_; i++) {
		delete[] grid[i];
	}
	delete grid;
	
	//Converting and publishing the PointCloud
	sensor_msgs::PointCloud2 output;
	pcl::toROSMsg(temp_cloud, output);
	pub.publish(output);
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "select_ground");
	ros::NodeHandle node;
	
	if(!node.getParam("heightmap/cell_size", cell_size_)) {
		node.setParam("heightmap/cell_size", 0.05);
		node.getParam("heightmap/cell_size", cell_size_);
	}
	if(!node.getParam("heightmap/grid_size", grid_size_)) {
		node.setParam("heightmap/grid_size", 500);
		node.getParam("heightmap/grid_size", grid_size_);
	}
	if(!node.getParam("heightmap/obstacle_dev", obstacle_dev_)) {
		node.setParam("heightmap/obstacle_dev", 100);
		node.getParam("heightmap/obstacle_dev", obstacle_dev_);
	}
	if(!node.getParam("heightmap/ground_dev", ground_dev_)) {
		node.setParam("heightmap/ground_dev", 0.09);
		node.getParam("heightmap/ground_dev", ground_dev_);
	}
	if(!node.getParam("heightmap/ground_height", ground_height_)) {
		node.setParam("heightmap/ground_height", -2);
		node.getParam("heightmap/ground_height", ground_height_);
	}
	if(!node.getParam("heightmap/ground_tol", ground_tol_)) {
		node.setParam("heightmap/ground_tol", 0.2);
		node.getParam("heightmap/ground_tol", ground_tol_);
	}
	
	pub = node.advertise<sensor_msgs::PointCloud2>("velodyne_ground", 1);
	ros::Subscriber sub = node.subscribe<sensor_msgs::PointCloud2>("velodyne_delete_car", 1, callback);	
	ros::spin();
}