#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <iostream>
#include <pcl/io/pcd_io.h>

#include <pcl/PCLPointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

#include <vector>
#include <cmath>

ros::Publisher pub;

float slice_height_, arch_distance_, arch_angle_;

void cloud_cb (const sensor_msgs::PointCloud2 input) {	
	pcl::PointCloud<pcl::PointXYZI> cloud;
	pcl::fromROSMsg(input, cloud);
	
	pcl::PointCloud<pcl::PointXYZI> cloud_aux = cloud;
	cloud_aux.points.clear();
	cloud_aux.width = 0;
	
	for(int i = 0; i < cloud.width; i++) {
		if(cloud.points[i].z < slice_height_) {
			if(!(pow(cloud.points[i].x, 2) + pow(cloud.points[i].y, 2) <= pow(arch_distance_, 2) && abs(cloud.points[i].y) <= cloud.points[i].x * tan(arch_angle_ * M_PI / 180) && cloud.points[i].x > 0)) {
				cloud_aux.points.push_back(cloud.points[i]);
				cloud_aux.width++;
			}
		}
	}
	
	sensor_msgs::PointCloud2 output;
	pcl::toROSMsg(cloud_aux, output);
	pub.publish(output);
}

int main (int argc, char** argv) {
	ros::init (argc, argv, "pointcloud2_to_pcd");
	ros::NodeHandle node;
	
	if(!node.getParam("heightmap/slice_height", slice_height_)) {
		node.setParam("heightmap/slice_height", -1);
		node.getParam("heightmap/slice_height", slice_height_);
	}
	if(!node.getParam("heightmap/arch_distance", arch_distance_)) {
		node.setParam("heightmap/arch_distance", 3.5);
		node.getParam("heightmap/arch_distance", arch_distance_);
	}
	if(!node.getParam("heightmap/arch_angle", arch_angle_)) {
		node.setParam("heightmap/arch_angle", 25);
		node.getParam("heightmap/arch_angle", arch_angle_);
	}
	
	ros::Subscriber sub = node.subscribe<sensor_msgs::PointCloud2>("velodyne_points", 1, cloud_cb);
	pub = node.advertise<sensor_msgs::PointCloud2>("velodyne_delete_car", 1);
	ros::spin ();
}